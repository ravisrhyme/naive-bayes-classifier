This is the implementation of Naive Bayes for classification of data in to Spam
or Ham. Done as a part of Assignment-1 for Natural Language Processing(CSCI-544)
course work at University of Southern California.


How to Run:

1. First train the classifier by running "python3 nblearn.py /path/to/directory/of/trainingdata"
2. It generates a model output file names nbmodel.txt.
3. Then run "python3 nbclassify.py /path/to/directory/of/testdata"
4. step #3 generates an output file names nboutput.txt besides printing metrics on console.

For more info please read CS544-assignment1.pdf present in the repository.

Training data can be found in spam_or_ham/train in repository
Test data can be found it spam_or_ham/dev in repository.


Happy using :)
