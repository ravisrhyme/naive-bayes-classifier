"""
name        : nbclassify.py
Author      : Ravi Kiran Chadalawada
Description : Please read README.md or CS544-assignment1.pdf with repo.

"""



import sys
import os
import pickle
import math

nbmodel_fileobject = open( "nbmodel.txt", "rb" )
count = pickle.load(nbmodel_fileobject)
nbmodel_fileobject.close()

#print (count['spam']['derived']+count['ham']['derived'])
#print (count['document_count']['spam'], ":", count['document_count']['ham'])

#number of documents of each type in training data
spam_document_count = count['document_count']['spam'];
ham_document_count  = count['document_count']['ham']


distinct_words_spam = len(count['spam']) #nothing but number of keys
distinct_words_ham = len(count['ham'])  #nothing but number of keys

#Total number of distinct keys in training set.

distinct_words_training_set = len(count['distinct_words'])

sum_of_spam_values = sum(count['spam'].values())
sum_of_ham_values  = sum(count['ham'].values())

document_prob_spam = spam_document_count/(spam_document_count + ham_document_count)
document_prob_ham = ham_document_count/(spam_document_count + ham_document_count)

#print ("prob_spam :", document_prob_spam, " prob_ham :", document_prob_ham)
#print ("sum_of_spam_values :",sum_of_spam_values)
#print ("sum_of_ham_values :", sum_of_ham_values)

#Counters

#number of documents of each type in test data
no_of_spam = 0
no_of_ham = 0

classified = 0
mismatch = 0

spam_as_spam = 0
ham_as_ham = 0
ham_as_spam = 0
spam_as_ham = 0

output_fileobject = open("nboutput.txt","w+")

for path, dirs, files in os.walk(str(sys.argv[1])):
	for filename in files:
		prob_of_word_spam = 0
		prob_of_word_ham = 0
		if "spam.txt" in filename:
			label = 'spam'
			no_of_spam += 1
		elif "ham.txt" in filename:
			label = 'ham'
			no_of_ham += 1
		else :
			continue;

		fullpath = os.path.join(path, filename)
		fd = open(fullpath, "r", encoding="latin1")
		string = fd.read()
		fd.close()
		tokens = string.split();
		
		for token in tokens:
			if token not in count['distinct_words'].keys():
				continue;
			if token in count['spam'].keys():
				prob_of_word_spam += math.log((count['spam'][token]+1)/(sum_of_spam_values + distinct_words_training_set))
			else:
				prob_of_word_spam += math.log((0+1)/(sum_of_spam_values + distinct_words_training_set))
			if token in count['ham'].keys():
				prob_of_word_ham += math.log(((count['ham'][token]+1)/(sum_of_ham_values + distinct_words_training_set)))
			else:
				prob_of_word_ham += math.log((0+1)/(sum_of_ham_values + distinct_words_training_set))


		spam_class_value = prob_of_word_spam + math.log(document_prob_spam)
		ham_class_value = prob_of_word_ham + math.log(document_prob_ham)

		if (spam_class_value > ham_class_value):
			# Add label filepath to opened file nboutput.txt
			line_spam = "spam "
			line_spam += path + "/"+ filename + "\n"
			output_fileobject.write(line_spam)

			if label == "spam":
				classified += 1;
				spam_as_spam += 1;
			else :
				mismatch += 1;
				ham_as_spam += 1;
				
		else:
			#Add label filepath to opened file nboutput.txt
			line_ham = "ham "
			line_ham +=  path + "/"+ filename + "\n"
			output_fileobject.write(line_ham)
		
			if label == "ham" :
				classified += 1
				ham_as_ham += 1
			else :
				mismatch += 1
				spam_as_ham += 1
	

output_fileobject.close()


# Commenting as final submission doesn't need these measures.

print ("Number of spam documents : ", no_of_spam)
print ("Number of ham documents : ", no_of_ham)

print ("Classified :" ,classified, " Mismatch :", mismatch)
print ("Spam-as-spam :",spam_as_spam,"Spam-as-ham :",spam_as_ham)
print ("ham-as-spam :",ham_as_spam,"ham-as-ham :",ham_as_ham)


accuracy = (classified/(classified+mismatch)) 

print ("Accuracy :", accuracy)

precision_of_spam = (spam_as_spam/(spam_as_spam + ham_as_spam))*100 
precision_of_ham  = (ham_as_ham/(ham_as_ham + spam_as_ham))*100

print ("precision_of_spam :",precision_of_spam,"%")
print ("precision_of_ham :", precision_of_ham,"%")

recall_of_spam = (spam_as_spam/no_of_spam)*100
recall_of_ham = (ham_as_ham/no_of_ham)*100

print ("recall_of_spam :",recall_of_spam,"%")
print ("recall_of_ham :", recall_of_ham,"%")

f1_spam = (2 * precision_of_spam * recall_of_spam)/(precision_of_spam+recall_of_spam)
f1_ham  = (2 * precision_of_ham * recall_of_ham)/(precision_of_ham+recall_of_ham)

print ("f1_spam :",f1_spam)
print ("f1_ham :", f1_ham)

