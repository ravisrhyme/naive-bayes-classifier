"""
name        : nblearn.py
Author      : Ravi Kiran Chadalawada
Description : Please read README.md or CS544-assignment1.pdf with repo.

"""


import sys
import os
import pickle

cmdargs = sys.argv

count = {"spam":{}, "ham":{}, "document_count":{'spam':0,'ham':0}, "distinct_words":{}}
total_count= {}

for path, dirs, files in os.walk(str(sys.argv[1])):
	for filename in files:
		# Setting the class label of file
		if "spam.txt" in filename:
			label = 'spam'
			count['document_count'][label] += 1
		elif "ham.txt" in filename:
			label = 'ham'
			count['document_count'][label] += 1
		else :
			continue;

		fullpath = os.path.join(path, filename)
		fd = open(fullpath, "r", encoding="latin1")
		string = fd.read()
		fd.close()
		tokens = string.split();
		for token in tokens:
			#print (token)
			if token in count[label].keys():
				count[label][token] += 1
			else :
				count[label][token] = 1

			if token in count['distinct_words'].keys():
				count['distinct_words'][token] += 1
			else :
				count['distinct_words'][token] = 1 

			if token in total_count.keys():
				total_count[token] += 1
			else:
				total_count[token] = 1

nbmodel_fileobject = open( "nbmodel.txt", "wb" )			
pickle.dump(count, nbmodel_fileobject)
nbmodel_fileobject.close()

"""
# Commenting as final submission doesn't need these calulated values.
#print (count['spam']['derived']+count['ham']['derived'],":",total_count['derived'])
print (count['document_count']['spam'], ":", count['document_count']['ham'])

sum_of_spam_values = sum(count['spam'].values())
sum_of_ham_values  = sum(count['ham'].values())

print ("sum_of_spam_values :",sum_of_spam_values)
print ("sum_of_ham_values :", sum_of_ham_values)

sum_of_spam_keys = len(count['spam'])
sum_of_ham_keys = len(count['ham'])

print ("Spam count :", sum_of_spam_keys)
print ("Ham count  :", sum_of_ham_keys)

print ("total_words :", len(total_count))

distinct_words = len(count['distinct_words'])
print ("distinct_words :", distinct_words)
"""
